<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5 </title>

    <style type="text/css">
        p {
            width: 150px;
        }
        div {
            margin-top: 20px;
        }
    </style>
    <script>
        <?php
            function imprimir() {
                
                $nombre = $_POST['nombre'];
                $apellido = $_POST['apellido'];
                $edad = $_POST['edad'];
                if ($nombre <> '' && $apellido <> '' && $edad > 0) {
                    $impresion = <<<TXT
                                Mi nombre es <b>$nombre $apellido</b> y tengo <b>$edad</b> años.
                            TXT;
                echo '<div>' . $impresion . '</div>';
                }
                else {
                    echo '<div><b>Complete todos los campos...</b></div>';
                }
            }
        ?>
    </script>
</head>
<body>
    <form action="ejercicio_5.php" method="post">
        <p>Inserte su Nombre: <input type="text" name="nombre" id="nombre"> </p>
        <p>Inserte su Apellido: <input type="text" name="apellido" id="apellido"> </p>
        <p>Inserte su Edad:<input type="number" name="edad" id="edad"> </p>
        <input type="submit" value="Imprimir" onclick="imprimir()">
    </form>
    <?php
        echo imprimir();
    ?>
</body>
</html>