<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3 </title>

    <style type="text/css">
        table {
            border-collapse: collapse;
            border: 2px solid black;
        }
        td{
            border: 1px solid black;
            text-align: center;
        }
    </style>
    
</head>
<body>
    <form action="ejercicio_3.php" method="post">
        Inserte un número: <input type="number" name="numero" id="numero">
        <input type="submit" value="Calcular" onclick="pulsar()">
    </form>
    <?php
        error_reporting(E_ALL ^ E_NOTICE);
        $n = $_POST['numero']; 

        if ($n > 0) {
            echo '<table>
                    <th>Numeros Pares</th>';   
        }

        for ($i=2; $i <= $n; $i=$i+2) { 
            if ($i%2 == 0) {
                echo '<tr><td>'.$i.'</td>
                     </tr>';
            }
        }
        echo '</table>'
    ?>
</body>
</html>