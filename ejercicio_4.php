<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4 </title>
</head>
<body>
    <?php
        function crear_semilla()
        {
          list($usec, $sec) = explode(' ', microtime());
          return (float) $sec + ((float) $usec * 100000);
        }
        mt_srand(crear_semilla());
        
        $A1 = mt_rand(50, 900);
        $A2 = mt_rand(50, 900);
        $A3 = mt_rand(50, 900);
        $aleatorios = array($A1, $A2, $A3); //llenamos el array con los numeros generados aleatoriamente

        rsort($aleatorios); //ordenamos el array descendentemente

        $A3 = array_pop($aleatorios);
        $S2 = array_pop($aleatorios);
        $A1 = array_pop($aleatorios);

        echo '<span style="color: green">' . $A1 . '</span> ';
        echo '<span>' . $A2 . '</span> ';
        echo '<span style="color: red">' . $A3 . '</span>';
    ?>
</body>
</html>