<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ejercicio 8 </title>
</head>
<body>
	<?php
		/*Cadena Heredoc, permite expandir variables en PHP*/
		$str=<<<HTML
			<form action="#" method="post">
				<div>
					<p><b>Generador de numeros aleatorios en filas y columnas. Inserte un número mayor a 0.</b></p>
					<label for="n">Inserte dimensión de las filas:</label>
					<input type="text" name="n" placeholder="" /><br>
					<label for="m">Inserte dimensión de las columnas:</label>
					<input type="text" name="m" placeholder="" />
				</div>
				<br/>
				<div class="button">
					<button type="submit">Generar</button>
				</div>
			</form>
		HTML;

		if (!isset($_POST['n']) && !isset($_POST['m']))
		{
			echo $str; //Imprimo el formulario cuando no me llega información por Post
		} else {
			$n = $_POST['n'];
			$m = $_POST['m'];
			
			$resultado = generarMatriz($n, $m);
			//echo  substr($palabra, 3, 1) . "<br>";
			//echo strlen($palabra) - 1 . "<br>";
			echo "Imprimiendo matriz de $n x $m con nros aleatorios: <br>";
			echo "<pre>$resultado</pre>";
			echo "<br/><br/><a href='ej3_8.php'>Volver al Generador</a><br/>";
		}

		function generarMatriz($n, $m)
		{
			$resultado = "";
			if (($n > 0) && ($m > 0)) 
			{
				for ($i = 0; $i < $n; $i++)
				{
					for ($j = 0; $j < $m; $j++)
					{
						$resultado .= rand(0,5000). "\t";
					}
					$resultado .= "\n";
				}
				return $resultado;
			} else {
				return "Debe insertar números positivos (mayor que cero).";
			}
		}
	?>
</body>
</html>