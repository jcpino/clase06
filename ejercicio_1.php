<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 1 </title>
</head>
<body>
  <?php
    $dos = 2; 
    $tres = 3; 
    $cubica = 1/3; 

    $x = ((sqrt($dos) * pi()) + pow($tres, $cubica)) / (M_EULER * M_E);

    echo '<b>El resultado de la fórmula x = ((A * ¶) + B) / (C*D) es: </b>' . $x . '</br></br>';
    echo '<b>Donde:</b>';
    echo '<ul>
            <li>A es la raiz cuadrada de 2.</li>
            <li>¶ es el número PI.</li>
            <li>B es es la raíz cúbica de 3.</li>
            <li>C es la constante de Euler.</li>
            <li>D es la constante e.</li>
          </ul>'
  ?>
</body>
</html>