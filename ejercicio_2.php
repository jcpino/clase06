<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 2 </title>
</head>
<body>
  <?php
    $Version = phpversion();
    $Id_version = PHP_VERSION_ID;
    $Max_enteros = PHP_INT_MAX;
    $Max_nombre = PHP_MAXPATHLEN;
    $So = PHP_OS;
    $Simbolo_fin = PHP_EOL;
    $Include_path = DEFAULT_INCLUDE_PATH;

    echo '<ul>
            <li><b>Versión de PHP utilizada:</b> ' . $Version . '</li>
            <li><b>El ID de la versión de PHP:</b> ' . $Id_version . '</li>
            <li><b>El valor máximo soportado para enteros para esa versión:</b> ' . $Max_enteros . '</li>
            <li><b>Tamaño máximo del nombre de un archivo:</b> ' . $Max_enteros . '</li>
            <li><b>Versión del Sistema Operativo:</b> ' . $So . '</li>
            <li><b>Símbolo correcto de "Fin De Línea" para la plataforma en uso:</b> ' . $Simbolo_fin . '</li>
            <li><b>El include path por defecto:</b> ' . $Include_path . '</li>
          </ul>';
  ?>
</body>
</html>